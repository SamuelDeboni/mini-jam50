extends KinematicBody2D

var dir : Vector2
var t := 0.0
var player
var tile_map : TileMap

signal on_hook_hit(pos)

func _ready():
	pass

func _process(delta):
	t += delta
	var collided := false
	var can_hook := false
	if $HookCast.is_colliding():
		var point : Vector2 = $HookCast.get_collision_point()
		var pos := point + dir*8
		var tilei = tile_map.get_cellv(tile_map.world_to_map(pos))
		can_hook = tilei == 0
		collided = true
 
	if can_hook:
		emit_signal("on_hook_hit", position)
	
	if t > 0.5 or collided:
		player.hook = null # VERY DANGEROUS!!!
		queue_free()

func _physics_process(delta):
	move_and_collide(dir * delta * 400)
