extends KinematicBody2D

export var acceleration : float = 500
export var decceleration : float = 8
export var max_speed : float = 200
export var gravity : float = 1000
export var jumping_force : float = -300
export(NodePath) var tile_map_path : NodePath
export(PackedScene) var hook_scene : PackedScene
export var warp_around_pos_right : float = 16*95
export var warp_around_pos_left : float = -16*10

var velocity : Vector2
var snap_vector := Vector2(0, 8)
var on_ground := false
var jump_count : int = 1
var g_mod : float
var input_force : float

var hooked : bool = false
var hook_position : Vector2
var hook : KinematicBody2D

var tile_map : TileMap

func _ready():
	tile_map = get_node(tile_map_path)

func _process(delta):	
	# Input processing
	input_force = (
			Input.get_action_strength("move_right") -
			Input.get_action_strength("move_left"))
	
	var hook_animation := hook != null or hooked
	
	if not hook_animation:
		# Animation
		if input_force > 0:
			$Sprite.flip_h = true
		elif input_force < 0:
			$Sprite.flip_h = false
		
		if not on_ground:
			$Sprite.animation = "jumping"
		elif input_force > 0 and velocity.x < 0 or input_force < 0 and velocity.x > 0:
			$Sprite.animation = "breaking"
		elif abs(velocity.x) > 10:
			$Sprite.animation = "walking"
		else:
			$Sprite.animation = "idle"
	else:
		var mouse_dir := get_local_mouse_position().normalized()
		var frame : int = floor((asin(mouse_dir.y) + PI * 0.5) * 5/PI)

		if mouse_dir.x > 0:
			$Sprite.flip_h = true
		else:
			$Sprite.flip_h = false
		
		if hook_animation and on_ground:
			$Sprite.animation = "hooked"
			$Sprite.frame = frame
		elif hook_animation:
			$Sprite.animation = "hooked"
			$Sprite.frame = frame + 5	

func _physics_process(delta):
	if is_on_floor():
		on_ground = true
		jump_count = 1
	else:
		on_ground = false

	process_hook(delta)
	if not hooked:
		process_movment(delta)
		process_gravity(delta)
		move_and_slide_with_snap(velocity, snap_vector, Vector2(0, -1))
	else:
		# wrap around the level
		if position.x > warp_around_pos_right + 16:
			hook_position.x -= position.x - warp_around_pos_left 
			position.x = warp_around_pos_left
		if position.x < warp_around_pos_left - 16:
			hook_position.x -= position.x - warp_around_pos_right
			position.x = warp_around_pos_right
		print(position.x)

		velocity = (hook_position - position).normalized() * 400
		move_and_slide(velocity, Vector2(0, -1))

func process_hook(delta: float):
	if Input.is_action_just_pressed("hook") and hook == null:
		hooked = false
		hook = hook_scene.instance()
		$"..".add_child(hook)
		var dir := get_local_mouse_position().normalized()
		var rot := -dir.angle_to(Vector2(1,0))
		hook.rotation = rot
		hook.position = position
		hook.dir = dir
		hook.player = self
		hook.tile_map = tile_map
		hook.connect("on_hook_hit", self, "_on_hook_hit")
	
	if hook != null:
		$HookLine.set_point_position(1, hook.position - position)
	elif hooked:
		$HookLine.set_point_position(1, hook_position - position)
	else:
		$HookLine.set_point_position(1, Vector2.ZERO)
	
	if Input.is_action_just_pressed("jump") and hooked:
		jump_count = 1
		jump()
		hooked = false
	
func process_gravity(delta: float):
	if velocity.y < 0 and Input.is_action_pressed("jump"):
		g_mod = 0.6
	elif velocity.y > 0 and Input.is_action_pressed("jump"):
		g_mod = 0.8
	elif velocity.y > 0:
		g_mod = 1.1
	else:
		g_mod = 1

	if not on_ground:
		snap_vector = Vector2(0,0)
		velocity.y += gravity * delta * g_mod
	else:
		snap_vector = Vector2(0,8)
		velocity.y = 0

func process_movment(delta):
	# wrap around the level
	if position.x > warp_around_pos_right + 16:
		if hook != null:
			hook.position.x -= position.x - warp_around_pos_left 
		position.x = warp_around_pos_left
	if position.x < warp_around_pos_left - 16:
		if hook != null:
			hook.position.x -= position.x - warp_around_pos_right
		position.x = warp_around_pos_right
	print(position.x)
	
	if abs(input_force) > 0:
		velocity.x += input_force * delta * acceleration
	elif is_on_floor():
		velocity.x -= velocity.x * delta * decceleration
	else:
		velocity.x -= velocity.x * delta * decceleration * 0.01
	velocity.x = clamp(velocity.x, -max_speed, max_speed)
	
	if is_on_ceiling():
		velocity.y = 5
	
	if is_on_wall():
		velocity.x = velocity.x / abs(velocity.x + 0.1)
	
	if Input.is_action_just_pressed("jump") and jump_count > 0:
		jump()
	

	
func jump():
	on_ground = false
	velocity.y = jumping_force
	jump_count -= 1

func _on_hook_hit(pos):
	hook_position = pos
	hooked = true
